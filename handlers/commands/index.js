'use strict';

const helpCommand = require('./help');
const createCommand = require('./create');
const joinCommand = require('./join');
const infoCommand = require('./info');

module.exports = {
	helpCommand,
	createCommand,
	joinCommand,
	infoCommand,
};
