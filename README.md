# anonybot
A telegram bot for unlimited anonymous groups

This bot is only as anonymous as you make it. Ensure you trust whoever runs it.
